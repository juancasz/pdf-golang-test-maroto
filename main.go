//Package main
//Barcodes are generated according to GS1 documentation and printed on pdf file
package main

import (
	"fmt"
	"os"
	"time"

	"pdf-test-maroto/codeGenerator"

	"github.com/johnfercher/maroto/pkg/consts"
	"github.com/johnfercher/maroto/pkg/pdf"
	"github.com/johnfercher/maroto/pkg/props"
)

func main() {
	fmt.Println("Creating PDF..")

	GTIN := "7701234000011"
	referenceNumber := "9876"
	paymentValue := "25500"
	maxPaymentDate := "20210605"

	//referenceNumber := "987619293912908919202132"
	//paymentValue := "25500143123"
	//maxPaymentDate := "20210605"
	//paymentValue := ""
	//maxPaymentDate := ""
	//referenceNumber := "22"

	GenerateBarcodePDF(GTIN, referenceNumber, paymentValue, maxPaymentDate, 30)
}

//GenerateBarcodePDF generate and print a barcode into a PDF
//GTIN: 13 characters length string. All the characters must be numbers. Required
//referenceNumber: string from 2 to 24 characters. All the characters must be numbers. Required
//paymentValue: string from 2 to 12 characters. Decimal notation allowed (For example 123.123). Optional. If not needed pass parameter as an empty string
//maxPaymentDate: 8 characters length string. Format AAAAMMDD. Optional. If not needed pass parameter as an empty string
//height: mm height of the barcode
func GenerateBarcodePDF(GTIN string, referenceNumber string, paymentValue string, maxPaymentDate string, height float64) {
	code, err := codeGenerator.GenerateCode(GTIN, referenceNumber, paymentValue, maxPaymentDate)

	fmt.Println("Creating barcode:", code)

	if err != nil {
		fmt.Println("barcode can't be generated:", err)
		return
	}

	begin := time.Now()
	m := pdf.NewMaroto(consts.Portrait, consts.A4)

	m.Row(height, func() {
		m.Col(12, func() {
			m.Barcode(code, props.Barcode{
				Percent: 100,
				Center:  true,
			})
		})
	})

	m.Row(3, func() {
		m.Col(12, func() {
			m.Text(code, props.Text{
				Size:   10,
				Family: consts.Arial,
				Style:  consts.Normal,
				Align:  consts.Center,
			})
		})
	})

	err = m.OutputFileAndClose("barcode.pdf")
	if err != nil {
		fmt.Println("Could not save PDF:", err)
		os.Exit(1)
	}

	end := time.Now()
	fmt.Println(end.Sub(begin))
}

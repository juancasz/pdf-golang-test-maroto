//package codeGenerator
//this package generates the string related to the barcode
package codeGenerator

import (
	"fmt"
	"strconv"
	"strings"
)

//generateCode generates the string code.
//GTIN: 13 characters length string. All the characters must be numbers. Required
//referenceNumber: string from 2 to 24 characters. All the characters must be numbers. Required
//paymentValue: string from 2 to 12 characters. Decimal notation allowed (For example 123.123). Optional. If not needed pass parameter as an empty string
//maxPaymentDate: 8 characters length string. Format AAAAMMDD. Optional. If not needed pass parameter as an empty string
func GenerateCode(GTIN string, referenceNumber string, paymentValue string, maxPaymentDate string) (string, error) {

	if !validateGTIN(GTIN) {
		return "", fmt.Errorf("invalid gtin13 code")
	}

	if !validateReferenceNumber(referenceNumber) {
		return "", fmt.Errorf("invalid reference number")
	}

	decimals, paymentString, isPaymentValid := validatePaymentValue(paymentValue)
	if !isPaymentValid {
		return "", fmt.Errorf("invalid payment value")
	}

	if !validateMaxPaymentDate(maxPaymentDate) {
		return "", fmt.Errorf("invalid maximum payment date")
	}

	code := "(415)" + GTIN + "(8020)" + referenceNumber

	if len(paymentValue) > 0 {
		code = code + "(390" + strconv.Itoa(decimals) + ")" + paymentString
	}

	if len(maxPaymentDate) > 0 {
		code = code + "(96)" + maxPaymentDate
	}

	return code, nil
}

func validateGTIN(GTIN string) bool {
	_, err := strconv.Atoi(GTIN)
	if err != nil {
		return false
	}
	return len(GTIN) == 13
}

func validateReferenceNumber(referenceNumber string) bool {
	_, err := strconv.ParseFloat(referenceNumber, 64)
	fmt.Println(err)
	if err != nil {
		return false
	}
	return len(referenceNumber) >= 2 && len(referenceNumber) <= 24
}

func validatePaymentValue(paymentValue string) (int, string, bool) {
	if len(paymentValue) > 0 {
		_, err := strconv.ParseFloat(paymentValue, 64)
		if err != nil {
			fmt.Println(err)
			return 0, "", false
		}
		parts := strings.Split(paymentValue, ".")
		if len(parts) < 2 {
			return 0, paymentValue, len(paymentValue) >= 2 && len(paymentValue) <= 12
		}
		return len(parts[1]), parts[0] + parts[1], len(paymentValue) >= 2 && len(paymentValue) <= 12
	}
	return 0, paymentValue, true
}

func validateMaxPaymentDate(maxPaymentDate string) bool {
	if len(maxPaymentDate) > 0 {
		_, err := strconv.Atoi(maxPaymentDate)
		if err != nil {
			return false
		}
		return len(maxPaymentDate) == 8
	}
	return true
}
